//Serial write symbols and meaning
/*
     '*' > Clockwise rotation
     '-' > CounterClockwise rotation
     '1' > Encoder 1 serial trigger
     '2' > Encoder 2 serial trigger
     '3' > Air on
     '4' > Air off
     '5' > Water on
     '6' > Water off
     's' > Stop signal
     Rest of serial is highByte and lowByte writes

     This comvention is followed to reduce the serial write data so as to reduce loop slowdown
*/

//Added line to check branch 'masterrr'
/*----------------------------------------------------------------------------------------------*/
//Define

#define air 6                      //Air switch on pin 6
#define wat 7                      //Water switch on pin 7
#define st 5                       //Stop command trigger
#define op 4                       //Signal for stop command (Connect 4->5 for triggering stop command) 
#define enIntB1 18                 //Pin 18 int > signal from channel B of Encoder 1 (denoted as B1) 
#define enIntA1 19                 //Pin 19 int > A1
#define enIntB2 20                 //Pin 20 int > B2
#define enIntA2 21                 //Pin 21 int > A2
#define enIntI2 3                  //Pin 3 int > I2 (Index channel)
//#define enB1 16                  //Shorted with enIntB1
//#define enB2 22                  //Shorted with enIntB2
#define cpr11 150                  //Counts per revolution for EncA1
#define cpr12 60                   //Counts per revolution for EncB1
#define cpr2 520                   //Counts per revolution for Enc2

/*-------------------------------------------------------------------------------*/
//Initialization

volatile long enA1cnt = 0;         //Encoder A1 counts
volatile long enB1cnt = 0;         //Encoder B1 counts
volatile long comp1 = 0;           //Compare for Enc1
volatile long enA2cnt = 0;         //Encoder A2 counts
volatile long enB2cnt = 0;         //Encoder B2 counts
volatile long enI2cnt = 0;         //Encoder I2 counts (Index pulses)
volatile long comp2 = 0;           //Compare for Enc2

float ang11 = 0.0;                 //Angle of rotation calculated from A1 counts
float ang12 = 0.0;                 //Angle of rotation calculated from B1 counts
float ang2 = 0.0;                  //Angle of rotation for Encoder2
static uint8_t enc_val = 0;        //Encoder lookup table variable
int eang1 = 0;                     //Integer angle counts
int eang2 = 0;
int dir1 = 2;                      //Direction indicator for en1
int dir2 = 2;                      //Direction indicator for en2

//Variables for air, water channels

int airSt, watSt;                  //Air and water switch states
int pa = HIGH, pw = HIGH;          //Previous air and water switch states
int stpSt = HIGH;                  //Stop command for debug/testing
int stpCnt = 0;                    //Stop command count (Part of air,water function)

int dbg = 628;                    //Debug variable
/*-------------------------------------------------------------------------------------*/
void setup() {
  //IO

  //pinMode(enB1,INPUT);           //See "Define" section for these pins
  //pinMode(enB2, INPUT);

  pinMode(enIntI2, INPUT);
  pinMode(enIntB2, INPUT);
  pinMode(enIntA2, INPUT);
  pinMode(enIntB1, INPUT);
  pinMode(enIntA1, INPUT);
  pinMode(air, INPUT_PULLUP);
  pinMode(wat, INPUT_PULLUP);
  pinMode(st, INPUT_PULLUP);
  pinMode(op, OUTPUT);

  //Attaching the interrupts
  attachInterrupt(digitalPinToInterrupt(enIntA1), enA1intf, CHANGE);
  attachInterrupt(digitalPinToInterrupt(enIntB1), enB1intf, CHANGE);
  attachInterrupt(digitalPinToInterrupt(enIntA2), enA2intf, RISING);
  attachInterrupt(digitalPinToInterrupt(enIntB2), enB2intf, RISING);
  attachInterrupt(digitalPinToInterrupt(enIntI2), enI2intf, RISING);

  //Sertial
  Serial.begin(9600);              //Using Serial 0
}


/*------------------------------------------------------------------------------------*/
//Main loop

void loop() {
  airSt = digitalRead(air);        //Reading air switch
  watSt = digitalRead(wat);        //Reading water switch
  stpSt = digitalRead(st);         //Reading stop state

  //AirWater section (Air, water and stop signals sent)
  airwater();

  //Encoder Count Section

  //Next line for debug, setting A1cnt = 0 during runtime
  //if (Serial.available()) if (Serial.read() == 'a') enA1cnt = 0;

  //Cal angles
  ang11 = (abs(enA1cnt) % cpr11) * 200.0 / cpr11;
  ang12 = (abs(enB1cnt) % cpr12) * 200.0 / cpr12;
  ang2 = (abs((-enA2cnt + enB2cnt)/2) % cpr2) * 360.0 / cpr2;
  eang1 = (int)(ang11 + 0.5);
  eang2 = (int)(ang2 + 0.5);
  //Uncomment to display beautifully (Not used during interface with C++)
  //enang1();
  //enang2();

  //Encoder 1
  if (abs(enA1cnt - comp1) > 0)
  {
    Serial.write('1');                            //Encoder 1 signal trigger
    if (enA1cnt < comp1) {
     Serial.write('*');
     dir1=0;}                                     //CW
    else  {
     Serial.write('-');
     dir1=1;}                                     //CCW
    Serial.write(highByte(eang1));
    Serial.write(lowByte(eang1));                 //Writing angles
    comp1 = enA1cnt;
  }
  //Encoder 2
  if (abs(enA2cnt - comp2) > 0)
  {
    Serial.write('2');                            //Encoder 2 signal trigger
    if (enA2cnt < comp2) {
     dir2=0;
     Serial.write('*');                           //CW
    }
    else  {
     Serial.write('-');                           //CCW
     dir2=1;
    }
    Serial.write(highByte(eang2));
    Serial.write(lowByte(eang2));                 //Writing angles
    comp2 = enA2cnt;
  }

  //Printing encoder counts (uncomment for debug)
  //countprnt1();
  //countprnt2();
 

  //delay(1);                                       // delay in between reads for stability
}

/*---------------------------------------------------------------------------------------------*/
//Other supplement functions

void airwater() {
  if (airSt != pa) {
    if (airSt == 1) Serial.write('4');
    else Serial.write('3');
    pa = airSt;
  }
  if (watSt != pw) {
    //Serial.print(" Water state changed to ");
    if (watSt == 1)
      Serial.write('6');
    else Serial.write('5');
    pw = watSt;
  }
  if (stpSt == LOW && stpCnt < 2) {
    Serial.write('s');
    Serial.write(highByte(dbg));
    Serial.write(lowByte(dbg));
    stpCnt++;
  }
}

//Printing encoder 2 counts
void countprnt2() {
  //a2count
  if (abs(enA2cnt - comp1) > 5)
  {
    //Serial.print("A2");
    //Serial.print(enA2cnt);
    comp1 = enA2cnt;
    Serial.print("\n I2 ");
    Serial.print(enI2cnt);
    Serial.print("\n");
  }
  //b2count
  if (abs(enB2cnt - comp2) > 5)
  {
    //Serial.print("B2");
    //Serial.print(enB2cnt);
    //Serial.print('\n');
    comp2 = enB2cnt;
  }
}

//Printing encoder 1 counts
void countprnt1() {
  //a2count
  if (abs(enA2cnt - comp1) > 5)
  {
    Serial.print("A2");
    Serial.print(enA2cnt);
    comp1 = enA2cnt;
  }
  //b2count
  if (abs(enB2cnt - comp2) > 5)
  {
    Serial.print("B2");
    Serial.print(enB2cnt);
    Serial.print('\n');
    comp2 = enB2cnt;
  }
}

//Encoder 1 signal details (CW/CCW, encoder counts, etc.)
void enang1() {
  if (abs(enA1cnt - comp1) > 0)
  {
    Serial.print("\n Encoder 1: ");
    if (enA1cnt < comp1) {
      Serial.print(" CW ");
    }
    else
      Serial.print(" CCW ");
    Serial.print(ang11);
    Serial.print(' ');
    //      Serial.print(ang12);
    //      Serial.print(' ');
    //      Serial.print(((abs(enA1cnt-enB1cnt)/2)%120)*360/120);
    Serial.print(enA1cnt);
    Serial.print(' ');
    Serial.print(enB1cnt); Serial.print(' ');
    Serial.print((enA1cnt - enB1cnt) / 2);
    Serial.print('\n');
    comp1 = enA1cnt;
  }
}

//Encoder 2 signal details (CW/CCW, angle, encoder counts, etc.)
void enang2() {
  if (abs(enA2cnt - comp2) > 2)
  {
    Serial.print("  En2:  ");
    if (enA2cnt < comp2) Serial.print("CW ");
    else Serial.print("CCW ");
    Serial.print(ang2);
    Serial.print('\n');
    Serial.print(enI2cnt);
    Serial.print('\n');
    comp2 = enA2cnt;
  }
}
//Encoder counts at index pulse
void encntint(){
     Serial.print("\n Encount at Index : ");
     Serial.print(enA2cnt);
     Serial.print(" ");
     Serial.print(enB2cnt);
     Serial.print(" ");
     Serial.print(enI2cnt);
     Serial.print(" end ");
     }
/*---------------------------------------------------------------------------------------------*/
//Interrupt service routines

void enA1intf() {
  static int8_t lookup_table[] = {0, 0, 0, -1, 0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0, 0};
  static uint8_t enc_val = 0;
  enc_val = enc_val << 2;
  enc_val = (enc_val | (digitalRead(enIntA1) << 1)) | digitalRead(enIntB1);
  enA1cnt += lookup_table[enc_val & 0b1111];
}

void enB1intf() {
  enB1cnt += ((digitalRead(enIntB1) == HIGH && digitalRead(enIntA1) == LOW) || (digitalRead(enIntB1) == LOW && digitalRead(enIntA1) == HIGH) ) ? 1 : -1; // For change reads
}

void enA2intf() {
  enA2cnt += (digitalRead(enIntA2) == HIGH && digitalRead(enIntB2) == LOW) ? +1 : -1;
}

void enB2intf() {
  enB2cnt += (digitalRead(enIntB2) == HIGH && digitalRead(enIntA2) == LOW) ? +1 : -1;
}

void enI2intf() {
  //encntint();                         //For checking counts at index
  enA2cnt=-10;
  enB2cnt=10;
  enI2cnt++;
}
/*--------------------------------------------------------------------------------------------*/

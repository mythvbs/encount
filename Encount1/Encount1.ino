//Encount based on look-up table.
#define air 6
#define wat 7
#define st 5
#define op 4
#define enIntB1 18
#define enIntA1 19
#define enIntB2 20
#define enIntA2 21
#define in1 3
#define enB1 16
#define enB2 22
#define cpr11 150
#define cpr12 60
#define cpr2 100            //Counts per revolution for Encoder2
#define enIntI2 3

volatile long enI2cnt = 0;     //Encoder A1 counts
volatile long enA1cnt = 0;     //Encoder A1 counts
volatile long enB1cnt = 0;     //Encoder B1 counts
volatile long comp1 = 0;     //Compare for Enc1
volatile long enA2cnt = 0;     //Encoder A2 counts
volatile long enB2cnt = 0;     //Encoder B2 counts
volatile long comp2 = 0;     //Compare for Enc2
float ang11 = 0.0; float ang12 = 0.0;
float ang2 = 0.0;   //Angles of rotation
int ch = 2;
static uint8_t enc_val = 0;
//For air water channel
int airSt, watSt;
int pa = HIGH, pw = HIGH;
int stpSt = HIGH;
int stpCnt = 0;

void setup() {
  //IO
  pinMode(in1, OUTPUT);
  //pinMode(enB1,INPUT);
   pinMode(enIntI2, INPUT);
  pinMode(enB2, INPUT);
  pinMode(enIntB2, INPUT);
  pinMode(enIntA2, INPUT);
  pinMode(enIntB1, INPUT);
  pinMode(enIntA1, INPUT);
  pinMode(air, INPUT_PULLUP);
  pinMode(wat, INPUT_PULLUP);
  pinMode(st, INPUT_PULLUP);
  pinMode(op, OUTPUT);

  //Int
  attachInterrupt(digitalPinToInterrupt(enIntA1), enA1intf, CHANGE);
  attachInterrupt(digitalPinToInterrupt(enIntB1), enB1intf, CHANGE);
  attachInterrupt(digitalPinToInterrupt(enIntA2), enA2intf, RISING);
  attachInterrupt(digitalPinToInterrupt(enIntB2), enB2intf, RISING);
  attachInterrupt(digitalPinToInterrupt(enIntI2), enI2intf, RISING);

  //Sertial
  Serial.begin(9600);
  //    Serial3.begin(9600);
  //Init
  //digitalWrite(in1, LOW);
  digitalWrite(op, LOW);
}

void loop() {
  airSt = digitalRead(air);
  watSt = digitalRead(wat);
  stpSt = digitalRead(st);

  //AirWater Section
  airwater();

  //Encoder Count Section

  if (Serial.available()) if (Serial.read() == 'a') enA1cnt = 0;
  //Cal angles
  ang11 = (abs(enA1cnt) % cpr11) * 200 / cpr11;
  ang12 = (abs(enB1cnt / 2) % cpr12) * 360 / cpr12;
  ang2 = (abs(enA2cnt) % cpr2) * 360 / cpr2;

  //Printing angles
  //ang1pe
  if (abs(enA1cnt - comp1) > 0)
  {
    Serial.print(" En1:  ");
    if (enA1cnt < comp1) {
      Serial.print("CW ");
      if (ch == 0) {
        Serial.print("change "); /*enA1cnt =0;*/
      } ch = 1;
    }
    else {
      Serial.print("CCW "); if (ch == 1) {
        Serial.print("change "); /*enA1cnt=0;*/
      } ch = 0;
    }
    Serial.print(ang11);
    Serial.print(' ');
    //      Serial.print(ang12);
    //      Serial.print(' ');
    //      Serial.print(((abs(enA1cnt-enB1cnt)/2)%120)*360/120);
    Serial.print(enA1cnt);
    Serial.print(' ');
    Serial.print(enB1cnt); Serial.print(' ');
    Serial.print((enA1cnt - enB1cnt) / 2);
    Serial.print(' ');
    Serial.print(enI2cnt);
    Serial.print('\n');
    comp1 = enA1cnt;
  }
  //ang2
  if (abs(enA2cnt - comp2) > 50)
  {
    Serial.print("  En2:  ");
    if (enA2cnt < comp2) Serial.print("CW ");
    else Serial.print("CCW ");
    Serial.print(ang2);
    Serial.print('\n');
    comp2 = enA2cnt;
  }

  // printing encoder counts (uncomment for debug)
  // countprnt();

  delay(5);        // delay in between reads for stability
}

void enA1intf() {
  static int8_t lookup_table[] = {0, 0, 0, -1, 0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0, 0};
  static uint8_t enc_val = 0;
  enc_val = enc_val << 2;
  enc_val = (enc_val | (digitalRead(enIntA1) << 1)) | digitalRead(enIntB1);
  enA1cnt += lookup_table[enc_val & 0b1111];



  //       enA1cnt += ((digitalRead(enIntA1)==HIGH && digitalRead(enIntB1)==LOW) || (digitalRead(enIntA1)== LOW && digitalRead(enIntB1)==HIGH) ) ? +1 : -1; // For change reads
  //enA1cnt += (digitalRead(enIntA1)==HIGH && digitalRead(enIntB1)==LOW) ? +1 : -1;  // for rising reads
  //enA1cnt += (digitalRead(enIntA1)==HIGH && digitalRead(enIntB1)==LOW) ? +1 : -1; //for fast reads
}

void enB1intf() {
  enB1cnt += ((digitalRead(enIntB1) == HIGH && digitalRead(enIntA1) == LOW) || (digitalRead(enIntB1) == LOW && digitalRead(enIntA1) == HIGH) ) ? 1 : -1; // For change reads
  //      enB1cnt += (digitalRead(enIntB1)==HIGH && digitalRead(enIntA1)==LOW) ? +1 : -1;  // for rising reads
  //enB1cnt += (digitalRead(enIntB1)==HIGH && digitalRead(enIntA1)==LOW) ? +1 : -1; // for fast reads
}

void enA2intf() {
  enA2cnt += (digitalRead(enIntA2) == HIGH && digitalRead(enIntB2) == LOW) ? +1 : -1;
}

void enB2intf() {
  enB2cnt += (digitalRead(enIntB2) == HIGH && digitalRead(enIntA2) == LOW) ? +1 : -1;
}

void enI2intf() {
  enI2cnt++ ;
}

void countprnt() {
  //a2count
  if (abs(enA2cnt - comp1) > 5)
  {
    Serial.print('A');
    Serial.print(enA2cnt);
    comp1 = enA2cnt;
  }
  //b2count
  if (abs(enB2cnt - comp2) > 5)
  {
    Serial.print('B');
    Serial.print(enB2cnt);
    Serial.print('\n');
    comp2 = enB2cnt;
  }
  delay(5);
}

void airwater() {
  if (airSt != pa) {
    //Serial.print(" Air state changed to ");
    if (airSt == 1) Serial.print(2);
    else Serial.print(3);
    pa = airSt;
    delay(100);
  }
  if (watSt != pw) {
    //Serial.print(" Water state changed to ");
    if (watSt == 1) Serial.print(1);
    else Serial.print(0);
    pw = watSt;
    delay(100);
  }
  if (stpSt == LOW && stpCnt < 2) {
    Serial.print(4);
    stpCnt++;
  }
}

